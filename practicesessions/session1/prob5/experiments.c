#include<stdio.h>

int fun(int);

int globalvar = 10;

int main()
{
	int i = 10;
	globalvar = 10;
	i = fun_local_var(10);
	printf("Local var: i: %d globalvar: %d\n", i, globalvar);
	
	i = 10;
	globalvar = 10;
	i = fun_global_var();
	printf("Global var: %d globalvar: %d\n", i, globalvar);

	i = 10;
	globalvar = 10;
	i = fun_global_var2();
	printf("Global var2: %d globalvar: %d\n", i, globalvar);
		
	return 0;
}

int fun_local_var(int j)
{
	// Notice that the value before the increment is returned
	return j++; 
}

int fun_global_var()
{
	// The value before the increment is returned, but the value
	// of the globalvar is increased before the instruction in
	// the main method is executed.
	return globalvar++; 
}

int fun_global_var2()
{
	return globalvar;
	globalvar++; // Never executed
}
