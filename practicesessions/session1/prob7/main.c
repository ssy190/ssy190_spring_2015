#include<stdio.h>

struct course {
	int courseno;
	char coursename[50];
};

int main()
{
	struct course c[] = {
		{1, "Modelling and Simulation"},
		{2, "Discrete Event Systems"},
		{3, "Linear Control System Design"},
		{4, "Embedded Control Systems"}
	};

	printf("%d ", c[1].courseno);
	printf("%s\n", (*(c + 3)).coursename);
	return 0;
}
