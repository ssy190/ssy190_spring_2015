#include<stdio.h>
int check(int, int);

int main()
{
	int c;
	c = check(10, 20);
	printf("c=%d\n", c);
	return 0;
}

int check(int i, int j)
{
	int *p, *q;
	p = &i;
	q = &j;
	printf("&i=%d, &j=%d\n", (int)&i, (int)&j);
	printf("p=%d, q=%d\n", (int)p, (int)q);
	printf("*p=%d, *q=%d\n", *p, *q);
	if (*p > *q)
		return *p;
	else
		return *q;
}
